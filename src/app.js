const Koa = require("koa");
const Router = require("koa-router");
const app = new Koa();

const PORT = process.env.PORT || 3030;

const route = new Router();

route.get("/healthcheck", ctx => {
  ctx.body = {
    timestamp: new Date().toISOString(),
    version: process.env.npm_package_version
  };
});

route.allowedMethods();
app.use(route.routes());

app.listen(PORT, err => {
  if (!err) {
    console.log(`Server running on port ${PORT}`);
  }
});
